# Quick and dirty import markdown to docx

If you're able to, use [pandoc](https://pandoc.org/) or [writage](http://www.writage.com/) (a Word plugin which uses pandoc).

Currently only parses markdown headers using the # syntax. Used to import markdown based outlines into word. 

# Usage
- Copy the VBA into a new module/macro in the word document
- Run the macro
- Select markdown file to import