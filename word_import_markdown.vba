' global regex objects - see initRegex
Dim isHeader As Object

Sub word_import_markdown()
' word_import_markdown imports a markdown (*.md) file into word and applies a limited set of markdown formatting to the document

Application.ScreenUpdating = False

Dim filePath As String, wdDoc As Document, txtFile As Document

' user choose file
filePath = chooseFile
If filePath = "" Then Exit Sub

'import file
loadFile (filePath)

' init regex patterns
Call initRegex
  
' format file
Call formatFile
  
' cleanup
Set txtFile = Nothing: Set wdDoc = Nothing
Application.ScreenUpdating = True
End Sub

Function formatFile()
' Formats each line of the active document

Dim wdDoc As Document, line As Paragraph, headerlevel As Integer
Set wdDoc = ActiveDocument

' format each line as applicable
For Each line In wdDoc.Paragraphs
  With line.range

    ' header lines
    If isHeader.test(.text) Then

        ' get the number of leading #
        headerlevel = Len(Split(.text, " ", 2)(0))
        ' remove leading # and space
        .text = Right(.text, Len(.text) - headerlevel - 1)
        ' format the line for heading
        .Style = wdDoc.Styles("Heading " & headerlevel)

    End If

  End With

Next line

End Function

Function loadFile(filePath As String)

' load file at path and import to active doc
Set wdDoc = ActiveDocument
Set txtFile = Documents.Open(FileName:=filePath, AddToRecentFiles:=False, Visible:=False, ConfirmConversions:=False)
wdDoc.range.InsertAfter txtFile.range.text & vbCr
txtFile.Close SaveChanges:=True

End Function

Function chooseFile() As String
    Dim fd As Office.FileDialog

    Set fd = Application.FileDialog(msoFileDialogFilePicker)

   With fd

      .AllowMultiSelect = False

      ' Set the title of the dialog box.
      .Title = "Choose file to import"

      ' Clear out the current filters, and add our own.
      .Filters.Clear
      .Filters.Add "MarkDown", "*.md"
      .Filters.Add "Text", "*.txt"
      .Filters.Add "All Files", "*.*"

      ' Show the dialog box. If the .Show method returns True, the
      ' user picked at least one file. If the .Show method returns
      ' False, the user clicked Cancel.
      If .Show = True Then
        chooseFile = .SelectedItems(1) 'export the first item

      End If
   End With
End Function


Function initRegex()
    ' set global regex patterns
    
    ' isHeader is a regex that matches headers
    ' the first word (characters before the first space) must consist of only # chars
    Set isHeader = CreateObject("VBScript.RegExp")
    isHeader.pattern = "#+ .*"
    
End Function
